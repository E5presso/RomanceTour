﻿using Core.Collections;

using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Core.Security
{
	/// <summary>
	/// AES 암호화 기능을 제공하는 클래스입니다.
	/// </summary>
	public class Aes : IEncryptionProvider
	{
		private const int IV_SIZE = 16;

		/// <summary>
		/// 대칭키를 가져옵니다.
		/// </summary>
		public byte[] Key { get; private set; }

		/// <summary>
		/// Aes 클래스를 초기화합니다.
		/// </summary>
		public Aes()
		{
			Key = KeyGenerator.GenerateBytes(32);
		}
		/// <summary>
		/// Aes 클래스를 초기화합니다.
		/// </summary>
		/// <param name="key">암호화에 사용할 키를 지정합니다.</param>
		public Aes(byte[] key)
		{
			byte[] _key = key;
			if (_key.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = _key;
		}
		/// <summary>
		/// Aes 클래스를 초기화합니다.
		/// </summary>
		/// <param name="key">암호화에 사용할 키를 지정합니다.</param>
		public Aes(string key)
		{
			byte[] _key = Base64.Decode(key);
			if (_key.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = _key;
		}
		/// <summary>
		/// Aes 클래스를 초기화합니다.
		/// </summary>
		/// <param name="key">암호화에 사용할 키를 지정합니다.</param>
		public Aes(Key key)
		{
			byte[] _key = key;
			if (_key.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = _key;
		}

		/// <summary>
		/// 새로운 키를 지정합니다.
		/// </summary>
		/// <param name="key">지정할 키입니다.</param>
		public void SetKey(byte[] key)
		{
			if (key.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = key;
		}
		/// <summary>
		/// 새로운 키를 지정합니다.
		/// </summary>
		/// <param name="key">지정할 키입니다.</param>
		public void SetKey(string key)
		{
			byte[] array = Encoding.UTF8.GetBytes(key);
			if (array.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = array;
		}
		/// <summary>
		/// 새로운 키를 지정합니다.
		/// </summary>
		/// <param name="key">지정할 키입니다.</param>
		public void SetKey(Key key)
		{
			byte[] array = key;
			if (array.Length > 32) throw new ArgumentException("키의 길이가 32바이트를 넘을 수 없습니다.");
			Key = array;
		}

		/// <summary>
		/// 데이터를 암호화합니다.
		/// </summary>
		/// <param name="data">암호화할 데이터입니다.</param>
		/// <param name="encrypted">암호화된 데이터입니다.</param>
		/// <returns>암호화 결과입니다.</returns>
		public bool Encrypt(byte[] data, out byte[] encrypted)
		{
			try
			{
				var buffer = new RingBuffer();
				var iv = Hash.Compute(data, HashAlgorithm.SHA512).Take(IV_SIZE).ToArray();
				using var aes = new RijndaelManaged
				{
					KeySize = 256,
					BlockSize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
					Key = Key,
					IV = iv
				};
				using var memory = new MemoryStream();
				using var crypto = new CryptoStream(memory, aes.CreateEncryptor(aes.Key, aes.IV), CryptoStreamMode.Write);
				crypto.Write(data, 0, data.Length);
				crypto.FlushFinalBlock();
				var cipher = memory.ToArray();

				buffer.Write(iv);
				buffer.Write(cipher);
				encrypted = buffer.ToArray();
				return true;
			}
			catch
			{
				encrypted = data;
				return false;
			}
		}
		/// <summary>
		/// 데이터를 복호화합니다.
		/// </summary>
		/// <param name="data">복호화할 데이터입니다.</param>
		/// <param name="decrypted">복호화된 데이터입니다.</param>
		/// <returns>복호화 결과입니다.</returns>
		public bool Decrypt(byte[] data, out byte[] decrypted)
		{
			try
			{
				var buffer = new RingBuffer(data);
				byte[] iv = buffer.Read(IV_SIZE);
				byte[] cipher = buffer.Flush();

				using var aes = new RijndaelManaged
				{
					KeySize = 256,
					BlockSize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
					Key = Key,
					IV = iv
				};
				using var memory = new MemoryStream();
				using var crypto = new CryptoStream(memory, aes.CreateDecryptor(aes.Key, aes.IV), CryptoStreamMode.Write);
				crypto.Write(cipher, 0, cipher.Length);
				crypto.FlushFinalBlock();
				decrypted = memory.ToArray();
				return true;
			}
			catch
			{
				decrypted = data;
				return false;
			}
		}
		/// <summary>
		/// 데이터를 암호화합니다.
		/// </summary>
		/// <param name="data">암호화할 데이터입니다.</param>
		/// <param name="encrypted">암호화된 데이터입니다.</param>
		/// <returns>암호화 결과입니다.</returns>
		public bool Encrypt(string data, out string encrypted)
		{
			try
			{
				bool result = Encrypt(Encoding.UTF8.GetBytes(data), out byte[] cipher);
				if (result) encrypted = Base64.Encode(cipher);
				else encrypted = data;
				return result;
			}
			catch
			{
				encrypted = data;
				return false;
			}
		}
		/// <summary>
		/// 데이터를 복호화합니다.
		/// </summary>
		/// <param name="data">복호화할 데이터입니다.</param>
		/// <param name="decrypted">복호화된 데이터입니다.</param>
		/// <returns>복호화 결과입니다.</returns>
		public bool Decrypt(string data, out string decrypted)
		{
			try
			{
				bool result = Decrypt(Base64.Decode(data), out byte[] plain);
				if (result) decrypted = Encoding.UTF8.GetString(plain);
				else decrypted = data;
				return result;
			}
			catch
			{
				decrypted = data;
				return false;
			}
		}
	}
}